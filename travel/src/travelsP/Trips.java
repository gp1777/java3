/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package travelsP;



/**
 *
 * @author 1279179
 */
public class Trips {
    private String destination;
    private String name;
    private String passno;
    private String departure;
    private String Return;
    
    public Trips(String destination, String name, String passno, String departure, String Return) {
        setDestination(destination);
        setName(name);
        setPassno(passno);
        setDeparture(departure);
        setReturn(Return);
    }
    
    
    public final void setDestination(String destination) {
        if ((destination.length() < 1) || destination.length() > 50 ) {
            throw new IllegalArgumentException ("Name must be in between 1-50 characters");
        }
        this.destination = destination;
    }
    
    public String getDestination() {
        return destination;
    }
    
    public final void setName(String name) {
        if (name.length() < 1 || name.length() > 50) {
            throw new IllegalArgumentException("Name must be between 1-50 characters");
        }
        this.name = name;
        
    }
    public String getName() {
        return name;
    }
    
    public final void setPassno(String passno) {
        
        if (!passno.matches("[a-zA-Z]{2}[0-9]{6}")) {
        throw new IllegalArgumentException ("Please use the format 'AA123456'");
    }
        
        
        this.passno = passno;
    }
    public String getPassno() {
        return passno;
    }
    
    public final void setDeparture(String departure) {
        if (!departure.matches("\\d{4}-\\d{2}-\\d{2}")) {
        throw new IllegalArgumentException ("Wrong format, please use yyyy-mm-dd");
    }
      
        this.departure = departure;
    }
    public String getDeparture() {
        return departure;
    }
    
    public final void setReturn(String Return) {
         if (!Return.matches("\\d{4}-\\d{2}-\\d{2}")) {
        throw new IllegalArgumentException ("Wrong format, please use yyyy-mm-dd");
    }
         int compare = Return.compareToIgnoreCase(departure);
        if(compare <= -1){
  throw new IllegalArgumentException ("Return date cannot be less than Departure date");
}

else{
    // strings are equal.
}
        this.Return = Return;
    }
    public String getReturn() {
        return Return;
    }
    
    @Override
    public String toString() {
        return String.format("Dest:(%s), Name:(%s), PassNo:(%s), Departure:(%s), Return:(%s)", destination, name, passno, departure, Return);
    }
   
}
