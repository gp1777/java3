/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package travelsP;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

/**
 *
 * @author 1279179
 */
public class Storage {
    
    static void  saveTripsToFile(ArrayList<Trips> list, File file) throws IOException {
         StringBuilder sb = new StringBuilder(64);
        try ( 
                
                PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(file + ".txt")))) {
            for (Trips t : list) {
                
               
                // Single line print
               // out.println(t.toString());
              
                out.print(t.getDestination() + ";");
                out.print(t.getName() + ";");
                 out.print(t.getPassno() + ";");
                 out.print(t.getDeparture() + ";");
                 out.print(t.getReturn() + ";");
                 out.println("");
                
            }
            
            
        }
        
    }
    
}
