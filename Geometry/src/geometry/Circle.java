/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package geometry;

import static java.lang.Math.PI;

/**
 *
 * @author ipd
 */
public class Circle extends GeoObj{
    private double radius;
    public Circle(String color, double radius ) {
        super(color);
        this.radius = radius;
    }

    @Override
    double getSurface() {
//        double area;
//        area = 2*PI*radius*radius;
        return 2*PI*radius*radius;
    }

    @Override
    double getCircumPerim() {
        //double circum = 2 * PI * radius;
        return 2 * PI * radius;
    }

    @Override
    int getVerticesCount() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    int getEdgesCount() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    String print() {
        String s = String.format("This circle is %s and has circum %.4f and the surface is %.4f", super.color, getCircumPerim(), getSurface());
        
        return s;
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String toString() {
        return print();
    }
    
    
}
