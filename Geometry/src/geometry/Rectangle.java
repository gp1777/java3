/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package geometry;

/**
 *
 * @author ipd
 */
public class Rectangle extends GeoObj {
    double length, width;
    
    public Rectangle(String color, double length){
        super(color);
        this.length = length;
    }

    public Rectangle(String color, double length, double width) {
        super(color);
        this.length = length;
        this.width = width;
    }

    @Override
    double getSurface() {
        return length * width;
    }

    @Override
    double getCircumPerim() {
        return 2 * (length + width);
    }

    @Override
    int getVerticesCount() {
        return 4;
    }

    @Override
    int getEdgesCount() {
        return 4;
    }

    @Override
    String print() {
        String s = String.format("Rectangle is %s, has %d edges and %d vertices, the area is %.4f and circum %.4f", super.color, getEdgesCount(),getVerticesCount(), getSurface(), getCircumPerim());
        return s;
    }

    @Override
    public String toString() {
        return print();
    }
    
}
