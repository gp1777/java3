/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package geometry;

/**
 *
 * @author ipd
 */
public class Square extends Rectangle {
    

    public Square(String color, double length) {
        super(color, length);
    }
    @Override
    double getSurface() {
        return length * length;
    }

    @Override
    double getCircumPerim() {
        return 4 * length;
    }

    @Override
    int getVerticesCount() {
        return 4;
    }

    @Override
    int getEdgesCount() {
        return 4;
    }

    @Override
    String print() {
        String s = String.format("Square is %s, has %d edges and %d vertices, the area is %.4f and circum %.4f", super.color, getEdgesCount(),getVerticesCount(), getSurface(), getCircumPerim());
        return s;
    }

    @Override
    public String toString() {
        return print();
    }
    
    
}
