/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package geometry;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;


/**
 *
 * @author ipd
 */
class ParsingException extends Exception {

    public ParsingException() {
        super();
    }

    public ParsingException(String message) {
        super(message);
    }

    public ParsingException(String message, Throwable cause) {
        super(message, cause);
    }
}

public class Geometry {
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        // TODO code application logic here
        ArrayList<GeoObj> geo = new ArrayList<>();

        try {
            Scanner fileInput = new Scanner(new File("input.txt"));
            while (fileInput.hasNext()) {
                String line = fileInput.nextLine();
                try {
                    String[] data = line.split(";");
//                    if (data.length != 3) {
//                        throw new ParsingException("Invalid number of fields, 3 expected");
//                    }
                    String temp = data[0].toLowerCase();
                    switch (temp) {
                        case "circle":
                            if (data.length != 3) {
                                throw new ParsingException("Invalid number of fields, 3 expected");
                            }
                            if (data[1].trim().length() == 0){
                                throw new ParsingException("Invalid color");
                            }
                            double radius = Double.parseDouble(data[2]);
                            Circle circle = new Circle(data[1], radius);
                            geo.add(circle);
                            
                            break;
                        case "hexagon":
                            if (data.length != 3) {
                                throw new ParsingException("Invalid number of fields, 3 expected");
                            }
                            if (data[1].trim().length() == 0){
                                throw new ParsingException("Invalid color");
                            }
                            radius = Double.parseDouble(data[2]);
                            Hexagon hexagon = new Hexagon(data[1], radius);
                            geo.add(hexagon);
                            break;
                        case "point":
                            if (data.length != 2){
                               throw new ParsingException("Invalid number of fields, 1 expected"); 
                            }
                            if (data[1].trim().length() == 0){
                                throw new ParsingException("Invalid color");
                            }
                            Point point = new Point(data[1]);
                            geo.add(point);
                            break;
                        case "rectangle":
                            if (data.length != 4){
                               throw new ParsingException("Invalid number of fields, 4 expected");                          
                            }
                            if (data[1].trim().length() == 0){
                                throw new ParsingException("Invalid color");
                            }
                            double length = Double.parseDouble(data[2]);
                            double width = Double.parseDouble(data[3]);
                            Rectangle rectangle = new Rectangle(data[1], length, width);
                            geo.add(rectangle);
                            break;
                        case "sphere":
                            break;
                        case "square":
                            if (data.length != 3){
                               throw new ParsingException("Invalid number of fields, 3 expected");                          
                            }
                            if (data[1].trim().length() == 0){
                                throw new ParsingException("Invalid color");
                            }
                            length = Double.parseDouble(data[2]);
                            Square square = new Square(data[1], length);
                            geo.add(square);
                            break;
                        default:
                            throw new ParsingException("Not valid shape");

                    }
                     
                     
                } catch (ParsingException | IllegalArgumentException e) {
                    System.out.println("Error parsing line: " + line);
                    System.out.println("  Reason: " + e.getMessage());

                }

            }
        } catch (IOException e) {
            /// e.printStackTrace();
            System.out.println("File reading error: " + e.getMessage());
        }
       for(GeoObj g : geo){
           System.out.println(g);
       }
       
       
       
    }
}
