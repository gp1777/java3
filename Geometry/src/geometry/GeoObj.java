/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package geometry;

/**
 *
 * @author ipd
 */
abstract public class GeoObj {
    String color;
    public GeoObj(String color){
        this.color = color;
    }
    abstract double getSurface();
    abstract double getCircumPerim();
    abstract int getVerticesCount();
    abstract int getEdgesCount();
    abstract String print();  
    
}

