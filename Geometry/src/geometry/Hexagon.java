/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package geometry;

/**
 *
 * @author ipd
 */
public class Hexagon extends GeoObj {
    double rad;
    public Hexagon(String color, double rad) {
        super(color);
        this.rad = rad;
    }

    @Override
    double getSurface() {
        return (3 * Math.sqrt(3) / 2) * rad;
    }

    @Override
    double getCircumPerim() {
        return 6 * rad;
    }

    @Override
    int getVerticesCount() {
        return 6;
    }

    @Override
    int getEdgesCount() {
        return 6;
    }

    @Override
    String print() {
        String s= String.format("this hexagon is %s has %d edges and %d vertice. The area is %.4f and the circum is %.4f", super.color, getEdgesCount(), getVerticesCount(), getSurface(), getCircumPerim());
    return s;
    }

    @Override
    public String toString() {
        return print();
    }
    
    
}
