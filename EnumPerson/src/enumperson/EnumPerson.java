/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package enumperson;


import java.io.File;
import java.io.FileNotFoundException;

import java.util.ArrayList;
import java.util.Scanner;





class Person {
    String name;
    Gender gender;
    AgeRange ageRange;
    
    enum Gender {
        Male, Female, NA
    }
    
    enum AgeRange {
        Below18, From18to35, From35to65, Over65
    }
        
    
    @Override
    public String toString() {
      return String.format("%s is a %s %s", name, gender, ageRange);
    }
}

// creating custom exception
class ParsingException extends Exception {
    public ParsingException() {
        super();
    }
}



public class EnumPerson {

    
    public static void main(String[] args) throws FileNotFoundException {
    
        ArrayList<Person> people = new ArrayList<>();
        
        Scanner fileInput = new Scanner(new File("C:\\exports/input.txt"));
        
        while (fileInput.hasNextLine()) {
            String line = fileInput.nextLine();
            String[] data = line.split(";");
            Person p = new Person();
            p.name = data[0];
            p.gender = Person.Gender.valueOf(data[1]);
            p.ageRange = Person.AgeRange.valueOf(data[2]);
            people.add(p);
        }
        for (Person p: people) {
            System.out.println(p.toString());
        }
        
                
        
    
    }
    
}
