/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package account;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;


public class Database {
    
    private final static String HOSTNAME = "localhost:3333";
    private final static String DBNAME = "Quiz3Account";
    private final static String USERNAME = "Quiz3Account";
    private final static String PASSWORD = "JYUPGQ5W5XAVvcuQ";

    private Connection conn;
    
    
    public Database() throws SQLException {
        conn = DriverManager.getConnection("jdbc:mysql://" + HOSTNAME + "/" + DBNAME, USERNAME, PASSWORD);
    }
    
     public void addTransaction (Transaction transaction) throws SQLException {
        String sql = "INSERT INTO transactions (date, withdrawal, deposit) VALUES (?, ?, ?)";
        try (PreparedStatement stmt = conn.prepareStatement(sql)) {
            stmt.setString(1, transaction.date.toString());
            stmt.setString(2, transaction.withdrawal.toPlainString());
            stmt.setString(3, transaction.deposit.toPlainString());
            
            
            stmt.executeUpdate();
        }
    }
    
    
    
    public ArrayList<Transaction> getAllTransactions() throws SQLException {
        String sql = "SELECT * FROM transactions";
        ArrayList<Transaction> list = new ArrayList<>();
        
        try (Statement stmt = conn.createStatement()) {
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
               
               
                Date date = result.getDate("date");
                BigDecimal withdrawal = result.getBigDecimal("withdraw");
                BigDecimal deposit = result.getBigDecimal("deposit");
             //   BigDecimal withdrawal = new BigDecimal(result.getString("withdrawal"));  
                     Transaction transaction = new Transaction(deposit, withdrawal, date);      
                list.add(transaction);
            } 
               // catch for parsing big decimal
        } catch (NumberFormatException e) {
                throw new SQLException ("Error parsing amount field", e);
            }
        return list;
    }
    
    public ArrayList<Transaction> getAccountTotalAmount() throws SQLException  {
        String sql = "SELECT SUM(deposit) FROM transactions";
      ArrayList<Transaction> list = new ArrayList<>();
      try (Statement stmt = conn.createStatement()) {
              
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                Date date = result.getDate("date");
                BigDecimal deposit = result.getBigDecimal("deposit");
                BigDecimal withdrawal = result.getBigDecimal("withdrawal");
           
                Transaction transaction = new Transaction (deposit, withdrawal, date);
                list.add(transaction);
            }
    } catch (NumberFormatException e) {
                throw new SQLException ("Error parsing amount field", e);
            }
   return list;
}
}