/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package account;

import java.math.BigDecimal;
import java.util.Date;


public class Transaction {
    
    BigDecimal deposit;
    BigDecimal withdrawal;
    Date date;

   
   public Transaction(BigDecimal deposit, BigDecimal withdrawal, Date date) {
        this.deposit = deposit;
        this.withdrawal = withdrawal;
        this.date = date;
    }
   
    
   
   
    public BigDecimal getDeposit() {
        return deposit;
    }

    public BigDecimal getWithdrawal() {
        return withdrawal;
    }

    public Date getDate() {
        return date;
    }
   
   @Override
    public String toString() {
        return String.format("On %s deposited %d", date, deposit);
    }
}
