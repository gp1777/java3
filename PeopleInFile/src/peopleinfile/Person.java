/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package peopleinfile;


public class Person {
    
    String name;
    int age;
    // TODO: enum Gender
    
    // constructor 
     public Person(String name, int age) {
         // calling setter in constructor
         setName(name);
         setAge(age);
    }
     
     // setter and getters for name and age
     // make setters final so you can override the exceptions
    public final void  setName(String fName)  {
        
        if ((fName.length() < 2) || fName.length() > 50) {
            throw new IllegalArgumentException ("Name must be  between 2-50 characters");
        }
        
        this.name = fName;
    }
         
    public String getName() {
        return this.name;
 
}
    public final void setAge(int Age) {
        this.age = Age;
        
        if ((Age < 0) || Age > 150) {
            throw new IllegalArgumentException ("Age must be between 0-150");
        }
       
    }
    public int getAge() {
        return this.age;
    }
    
    @Override
    public String toString() {
        return String.format("%s is %d years old", name, age);
    }
   
}