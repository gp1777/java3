/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package todoP;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;

/**
 *
 * @author GP
 */
public class Storage {
    
     public static final String FILE_NAME = "data.txt";
    
    static void saveTasksToFile(ArrayList<Todo> list) throws IOException {
          // FIXME: use try-with-resources here
               PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(FILE_NAME)));
                for (Todo td : list) {
                    out.println(td.getTask());
                    out.println(td.getDueDate());
                    out.println(td.getIsDone());
                     }
                out.close();       
   }
    
   
    static ArrayList<Todo> loadTasksFromFile() throws IOException {
        ArrayList<Todo> list = new ArrayList<>();
        try {
            File file = new File(FILE_NAME);
            if (!file.exists()) {
                return list;
            }
            Scanner scn = new Scanner(file);
            while (scn.hasNextLine()) {
                String Task = scn.nextLine();
            String DueDate = scn.nextLine();
            boolean IsDone = scn.nextBoolean();
            scn.nextLine();
                Todo p = new Todo(Task, DueDate, IsDone);
                list.add(p);
            }
        } catch (InputMismatchException e) {
            throw new IOException("Error while parsing data file", e);
        } catch (IllegalArgumentException e) {
            throw new IOException("Error while parsing data file", e);
        }        
        return list;
    }
}
