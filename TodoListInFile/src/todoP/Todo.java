/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package todoP;

/**
 *
 * @author GP
 */
public class Todo {
 
   private String task;
  private  String dueDate;
   private boolean isDone;
    
    // constructor
    public Todo(String task, String dueDate, boolean isDone) {
         // calling setter in constructor
         setTask(task);
         setDueDate(dueDate);
         setIsDone(isDone);
         
    }
    
     public final void setTask(String task) {
        this.task = task;
        
        // exception
        if ((task.length() < 1) || task.length() > 100) {
            throw new IllegalArgumentException ("Task must be between 1-100 characters");
        }
        
    }

    public final void setDueDate(String dueDate) {
        this.dueDate = dueDate;
        
       
   
     if (!dueDate.matches("\\d{4}-\\d{2}-\\d{2}")) {
        throw new IllegalArgumentException ("Wrong format, please use yyyy-mm-dd");
    }
      
       
    }

    public final void setIsDone(boolean isDone) {
        this.isDone = isDone;
    }

    public String getTask() {
        return task;
    }

    public String getDueDate() {
        return dueDate;
    }

    public boolean getIsDone() {
        return isDone;
    }
    
    @Override
    public String toString() {
        return String.format("%s due on %s, is completed %b", task, dueDate, isDone);
    }
    
    
    
}
