/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package todoP;

/**
 *
 * @author GP
 */
public interface UsableInterface<T> {
    
    public String saveToString(); 
    
    public T readFromString(String data);
    
    
}


