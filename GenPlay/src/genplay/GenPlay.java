/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package genplay;



class Car implements Comparable<Car> {
    String make;
    String model;
    int maxSpeedKmph;
    double secTo100kmph;
    double litersPer100km;
    private static int counter = 0;
    
    public static int getCount() {
        
        return counter;
    }
    
    // constructor
     Car(String make, String model, int maxSpeedKmph, double secTo100kmph, double litersPer100km) {
         this.make = make;
         this.model = model;
         this.maxSpeedKmph = maxSpeedKmph;
         this.secTo100kmph = secTo100kmph;
         this.litersPer100km = litersPer100km;
         counter++;
    }
    
     
     @Override
    public int compareTo(Car cr) {
        return cr.maxSpeedKmph - maxSpeedKmph;
    }
}

public class GenPlay {

  
    public static void main(String[] args) {
        
       
	Stack<String> stackOfStrings = new Stack<>();
	
	System.out.printf("Stack of strings is %d tall\n", 
		stackOfStrings.getHeight());
        
        stackOfStrings.push("Adam");
         stackOfStrings.push("Berry");
          stackOfStrings.push("Jerry");
           stackOfStrings.push("Bill");
	

	System.out.printf("Stack of strings is %d tall\n", 
	stackOfStrings.getHeight());
          
	for (int i=0; i<4; i++) {
            String s =  stackOfStrings.pop();
            System.out.println("Popped " + s);
        }
    }
    
}
