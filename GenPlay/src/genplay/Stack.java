/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package genplay;

import java.util.ArrayList;


public class Stack<T> {
    ArrayList<T> array = new ArrayList<>();
    
	public Stack() {
            
        }
        public int getHeight() {
            return array.size();
        }
        public void push(T item) {
            // insert new element at the beginning of the array
            array.add(0, item);
            
        }
        public T pop() {
            if (array.isEmpty()) {
                return null;
            }
            // remove the element from the beginning of the array
            return array.remove(0);
            
        }
    
}
