/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package effectivesorting;

import java.util.ArrayList;


/**
 *
 * @author 1279179
 */
public class Person implements Comparable<Person> {

   String name;
   int age;

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }
   
   Person (String name, int age) {
       this.name = name;
       this.age = age;
   }
   
   public int compareTo(Person prs) {
       //int compareAge = name.compareTo(prs.name);
       //return(prs.age);
       int compareAge = ((Person) prs).getAge();
       
       return this.age - compareAge;
   }
   
 
   
   
   static ArrayList<Person> people = new ArrayList<Person>();

   
    public static void main(String[] args) {
        
        people.add( new Person("John", 30));
        people.add( new Person("Bob", 80)); 
        people.add( new Person("Marry", 35));
        people.add( new Person("Bill", 60));
        people.add( new Person("Jerry", 35));
        people.add( new Person("Dale", 50));
        people.add( new Person("Susan", 30));
        people.add( new Person("Smith", 80));
        
      
      
        
    }
    
}
