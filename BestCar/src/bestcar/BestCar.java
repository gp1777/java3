/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bestcar;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;




class Car implements Comparable<Car> {
    String make;
    String model;
    int maxSpeedKmph;
    double secTo100kmph;
    double litersPer100km;
    private static int counter = 0;
    private int objectId = 0;
    
    
    public static int getCount() {
        
        return counter;
    }
    
    public int getUniqueId() {
         return objectId;
     }
    
    // constructor
     Car(String make, String model, int maxSpeedKmph, double secTo100kmph, double litersPer100km) {
         this.make = make;
         this.model = model;
         this.maxSpeedKmph = maxSpeedKmph;
         this.secTo100kmph = secTo100kmph;
         this.litersPer100km = litersPer100km;
        counter++;
      //  this.objectId = counter++;
      objectId = counter;
      
         
    }
    
     
     @Override
    public int compareTo(Car cr) {
        return cr.maxSpeedKmph - maxSpeedKmph;
    }
}

  class CarComparatorByMaxSpeed implements Comparator<Car> {
      @Override
      public int compare(Car cr1, Car cr2) {
           
           
            return cr1.maxSpeedKmph - cr2.maxSpeedKmph;
      }
  }

class CarComparatorByAccelleration implements Comparator<Car> {
    @Override
    public int compare(Car cr1, Car cr2) {
            return (int) (cr1.secTo100kmph - cr2.secTo100kmph);
    }
}

class CarComparatorByEconomy implements Comparator<Car> {
    @Override
    public int compare (Car cr1, Car cr2) {
       
            return (int) (cr1.litersPer100km - cr2.litersPer100km);
    }
}

class CarComparatorBestWorst implements Comparator<Car> {
    
    @Override
    public int compare (Car cr1, Car cr2) {
       int result = cr1.make.compareTo(cr2.make);
       if (result != 0) return result;
       int result2 = cr1.model.compareTo(cr2.model);
       if (result2 != 0) return result2;
       return (int) (cr1.maxSpeedKmph - cr2.maxSpeedKmph);
      
    }
}




public class BestCar {
    

    static ArrayList<Car> garage = new ArrayList<>();
   
    public static void main(String[] args) {
        
        garage.add(new Car("Toyota", "Corolla", 150, 15, 5));
        garage.add(new Car("Honda", "Civic", 190, 12, 6));
        garage.add(new Car("Subaru", "Impreza", 210, 11, 5));
        garage.add(new Car("Ford", "Focus", 200, 15, 5));
        garage.add(new Car("Mitsubishi", "eclipse", 230, 10, 10));
        garage.add(new Car("Nissan", "350z", 300, 6, 8));
        garage.add(new Car("Toyota", "Camry", 200, 12, 5));
        garage.add(new Car("Honda", "Accord", 210, 11, 3));
        garage.add(new Car("Dodge", "Charger", 280, 9, 2));
        garage.add(new Car("Ford", "Mustang", 170, 8, 5));
        
        
      
        Collections.sort(garage, new CarComparatorByMaxSpeed());
        System.out.println("BY MAX SPEED"); 
        for (Car c : garage) {
        System.out.printf("%s, Top Speed:%d, Secto100:%f, liters:%f\n", c.make, c.maxSpeedKmph, c.secTo100kmph, c.litersPer100km);
        }
        System.out.println("");
         
         System.out.println("BY ACCELERATION"); 
         Collections.sort(garage, new CarComparatorByAccelleration());
         for (Car c : garage) {
        System.out.printf("%s, Top Speed:%d, Secto100:%f, liters:%f\n", c.make, c.maxSpeedKmph, c.secTo100kmph, c.litersPer100km);
        }
          System.out.println("");
         System.out.println("BY ECONOMIC"); 
         Collections.sort(garage, new CarComparatorByEconomy());
         for (Car c : garage) {
        System.out.printf("%s, Top Speed:%d, Secto100:%f, liters:%f\n", c.make, c.maxSpeedKmph, c.secTo100kmph, c.litersPer100km);
        }
         
         System.out.println("");
         
         System.out.println("BEST TO WORST");
          Collections.sort(garage, new CarComparatorBestWorst());
         for (Car c : garage) {
         
        System.out.printf("%s, %s, Top Speed:%d, Secto100:%f, liters:%f \n" , c.make, c.model, c.maxSpeedKmph, c.secTo100kmph, c.litersPer100km);
        }
         System.out.println("TOTAL CARS:");
        System.out.println(Car.getCount());
       
   
        
    }
    
}
