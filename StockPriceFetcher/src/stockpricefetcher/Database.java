/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stockpricefetcher;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;



public class Database {
    // TjttLMMQH3z8zPtG
    
     private final static String HOSTNAME = "localhost:3333";
    private final static String DBNAME = "stockpricefetcher";
    private final static String USERNAME = "stockpricefetcher";
    private final static String PASSWORD = "TjttLMMQH3z8zPtG";

    private Connection conn;
    
    
    public Database() throws SQLException {
        conn = DriverManager.getConnection("jdbc:mysql://" + HOSTNAME + "/" + DBNAME, USERNAME, PASSWORD);
    }
    
     // CRUD
       public void addStock(Stock stock) throws SQLException {
        String sql = "INSERT INTO stocks (symbol, name, price) VALUES (?, ?, ?)";
        try (PreparedStatement stmt = conn.prepareStatement(sql)) {
            stmt.setString(1, stock.symbol);
            stmt.setString(2, stock.name);
            stmt.setString(3, stock.price.toPlainString());
            
            stmt.executeUpdate();
        }
    }
    
    public ArrayList<Stock> getAllStocks() throws SQLException {
        String sql = "SELECT * FROM stocks";
        ArrayList<Stock> list = new ArrayList<>();
        
        try (Statement stmt = conn.createStatement()) {
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int id = result.getInt("id");
                String symbol = result.getString("symbol");
                java.sql.Date dueDateSql = result.getDate("dueDate");
                String isDoneStr = result.getString("isDone");
                Stock stock = new Stock(id, symbol, dueDateSql, isDoneStr);
                list.add(todo);
            }
    }
        return list;
    }
//    
//     public void updateStock(Stock stock) throws SQLException {
//         String sql = "UPDATE todos SET task=?, dueDate=?, isDone=? WHERE id=?";
//        try (PreparedStatement stmt = conn.prepareStatement(sql)) {
//            stmt.setString(1, stock.getTask());
//            stmt.setDate(2, stock.getDueDateSql());
//            stmt.setString(3, stock.isDoneString());
//            // where is the last parameter
//            stmt.setInt(4, stock.getId());
//            
//            stmt.executeUpdate();
//        }
//    }
    
    
}
