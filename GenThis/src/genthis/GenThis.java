/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package genthis;

import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author 1279179
 */
public class GenThis {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
     
        try {
        ArrayList list = new ArrayList();
        // no control over what you put into the list
        list.add("Jerry was here");
        list.add(new Object());
        list.add(new Scanner(System.in));
        
        String DATA = "Jerry White\nMarry Moe\n";
        Scanner input = new Scanner(DATA);
        String n1 = input.nextLine();
        System.out.printf("n1: %s", n1);
        
//        for (int i=0; i<list.size(); i++) {
//            Object o = list.get(i);
//            String s = (String) o;
//            System.out.println(s);
//        }
        } catch (ClassCastException e) {
            System.out.println("The old way blowed up: " + e.getMessage());
        }
    {   // new way using generics
        ArrayList<String> list = new ArrayList();
        list.add("Jerry was here");
       // list.add(new Object());
         for (int i=0; i<list.size(); i++) {
            Object o = list.get(i);
            String s = (String) o;
           // System.out.println(s);
        }
    }
}
} 

