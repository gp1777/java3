/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package traveldb;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author GP
 */
public class Database {
    private final static String HOSTNAME = "localhost:3306";
    private final static String DBNAME = "traveldb";
    private final static String USERNAME = "traveldb";
    private final static String PASSWORD = "N3hR1yQfx88Mjc1O";
    
    private Connection conn;
    
    public Database() throws SQLException {
        conn = DriverManager.getConnection("jdbc:mysql://" + HOSTNAME + "/" + DBNAME, USERNAME, PASSWORD);
    }
    
    // CRUD
       public void addTrips(Trips trips) throws SQLException {
        String sql = "INSERT INTO travels (destination, name, passno, departure, returndate) VALUES (?, ?, ?, ?, ?)";
        try (PreparedStatement stmt = conn.prepareStatement(sql)) {
            stmt.setString(1, trips.getDestination());
            stmt.setString(2, trips.getName());
            stmt.setString(3, trips.getPassno());
            stmt.setString(4, trips.getDeparture());
            stmt.setString(5, trips.getReturn());
            
            stmt.executeUpdate();
        }
    }
    
    public ArrayList<Trips> getAllTrips() throws SQLException {
        String sql = "SELECT * FROM travels";
        ArrayList<Trips> list = new ArrayList<>();
        
        try (Statement stmt = conn.createStatement()) {
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int id = result.getInt("id");
                String destination = result.getString("destination");
                String name = result.getString("name");
                String passno = result.getString("passno");
                String departure = result.getString("departure");
                String returndate = result.getString("returndate");
                
                Trips trips = new Trips(id, destination, name, passno, departure, returndate);
                list.add(trips);
            }
    }
        return list;
    }
    
    public Trips getTripsById(int id) throws SQLException {
        throw new RuntimeException("TODO: method not implemented");
    }
    
    public void updateTrips(Trips todo) throws SQLException {
         throw new RuntimeException("TODO: method not implemented");
    }
    
    public void deleteTripsById(int id) throws SQLException {
         throw new RuntimeException("TODO: method not implemented");
    }
    
}
