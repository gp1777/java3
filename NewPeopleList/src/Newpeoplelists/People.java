/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Newpeoplelists;

/**
 *
 * @author 1279179
 */
public class People {
    
    private String name;
    private int age;
    private String postcode;
    
    // constructor
    public People(String name, int age, String postcode) {
        setName(name);
        setAge(age);
        setPostCode(postcode);
    }
    
    // name getter and setters
    public final void setName(String name) {
        this.name = name;
        
        if ((name.length() < 2) || name.length() > 20 ) {
            throw new IllegalArgumentException ("Name must be in between 2-20 characters");
        }
    }
    public String getName() {
        return name;
    }
    
    // age getter and setters
    public final void setAge(int age) {
        this.age = age;
        
        if ((age < 0) || age > 150) {
            throw new IllegalArgumentException ("Age must be in between 0-150");
        }
    }
    public int getAge() {
        return this.age;
    }
    
    // postcode getter and setters
    public final void setPostCode(String postcode) {
        this.postcode = postcode;
        
        if (!postcode.matches("[A-VXY][0-9][A-Z] ?[0-9][A-Z][0-9]")) {
        throw new IllegalArgumentException ("Please use the format (L1L 1L1)");
    }
    }
    public String getPostCode() {
        return this.postcode;
    }
    
    @Override
    public String toString() {
        return String.format("%s is %d years old, Postal Code is %s", name, age, postcode);
    }
}
