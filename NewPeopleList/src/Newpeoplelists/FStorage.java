/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Newpeoplelists;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;



public class FStorage {
    
  //   public static final String FILE_NAME = "data.txt";
     
     
     static void  savePeopleToFile(ArrayList<People> list, File file) throws IOException {
         
        // FIXME: use try-with-resources here
    
        PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(file)));
         
        for (People p : list) {
            // new line print
            out.println(p.getName());
            out.println(p.getAge());
            out.println(p.getPostCode());
          
          // Single line print
         // out.println(p.toString());
        }
        out.close();
        
    }
     
      static ArrayList<People> loadPeopleFromFile(File file) throws IOException {
        ArrayList<People> list = new ArrayList<>();
        try {
           
            if (!file.exists()) {
                return list;
            }
            Scanner in = new Scanner(file);
            while (in.hasNextLine()) {
                String name = in.nextLine();
                int age = in.nextInt();
                in.nextLine(); // consume the left-over newline character
                String postcode = in.nextLine();
                People p = new People(name, age, postcode);
                list.add(p);
            }
        } catch (InputMismatchException e) {
            throw new IOException("Error while parsing data file", e);
        } catch (IllegalArgumentException e) {
            throw new IOException("Error while parsing data file", e);
        }        
        return list;
    }
}
